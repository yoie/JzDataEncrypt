unit AES;

interface

uses
  System.SysUtils, System.Classes, System.Math, ElAES;

const
  SDestStreamNotCreated = 'Dest stream not created.';
  SEncryptStreamError = 'Encrypt stream error.';
  SDecryptStreamError = 'Decrypt stream error.';

type
  TKeyBit = (kb128, kb192, kb256);

function EncryptString(Value: string; Key: string; KeyBit: TKeyBit = kb128): string;
function DecryptString(Value: string; Key: string; KeyBit: TKeyBit = kb128): string;



implementation


function StrToHex(Const str: String): string;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to Length(str) do
    Result := Result + IntToHex(Ord(str[I]), 2);
end;

function HexToStr(const Str: String;OutBuffer:TStream): Integer;
var
  I,c: Integer;
begin
  for I := 1 to Length(Str) do
  begin
    if ((I mod 2) = 1) then
      begin
        c:=StrToInt('0x'+ Copy(Str, I, 2));
        OutBuffer.Write(c,1);
      end;
  end;
  Result:=OutBuffer.Size;
end;

function EncryptString(Value: string; Key: string;
  KeyBit: TKeyBit = kb128): string;
var
  SS,DS: TMemoryStream;
  Size: Int64;
  AESKey128: TAESKey128;
  AESKey192: TAESKey192;
  AESKey256: TAESKey256;


  Buffer:Tbytes;
  i:Integer;
  KeyBuffer:array [0..100] of Byte;

begin
  Result := '';
  //Unitcode转Ansi
  Buffer:=TEncoding.Convert(TEncoding.Unicode,TEncoding.ANSI,WideBytesOf(Value));
  ss := TMemoryStream.Create;
  SS.Write(Buffer,Length(Buffer));
  DS := TMemoryStream.Create;
  try
    Size := SS.Size;
    DS.WriteBuffer(Size, SizeOf(Size));
    //key转成Ansi
    for i := 1 to  Length(Key) do KeyBuffer[i - 1]:=  Ord(Key[i]);
     {  --  128 位密匙最大长度为 16 个字符 --  }
    if KeyBit = kb128 then
    begin
      FillChar(AESKey128, SizeOf(AESKey128), 0 );
      Move(KeyBuffer, AESKey128, Min(SizeOf(AESKey128), Length(Key)));
      EncryptAESStreamECB(SS, 0, AESKey128, DS);
    end;
    {  --  192 位密匙最大长度为 24 个字符 --  }
    if KeyBit = kb192 then
    begin
      FillChar(AESKey192, SizeOf(AESKey192), 0 );
      Move(KeyBuffer, AESKey192, Min(SizeOf(AESKey192), Length(Key)));
      EncryptAESStreamECB(SS, 0, AESKey192, DS);
    end;
    {  --  256 位密匙最大长度为 32 个字符 --  }
    if KeyBit = kb256 then
    begin
      FillChar(AESKey256, SizeOf(AESKey256), 0 );
      Move(KeyBuffer, AESKey256, Min(SizeOf(AESKey256), Length(Key)));
      EncryptAESStreamECB(SS, 0, AESKey256, DS);
    end;
      SetLength(Buffer,Ds.Size);
      DS.Position := 0;
      DS.Read(Buffer,DS.Size);
      for I := 0 to DS.Size - 1 do Result := Result + IntToHex(Buffer[I], 2);
  finally
    SS.Free;
    DS.Free;
  end;
end;


{  --  字符串解密函数 默认按照 128 位密匙解密 --  }
function DecryptString(Value: String; Key: String; KeyBit: TKeyBit = kb128): String;
var
  SS, DS: TStringStream;
  Size: Int64;
  AESKey128: TAESKey128;
  AESKey192: TAESKey192;
  AESKey256: TAESKey256;

  i:Integer;
  KeyBuffer:array [0..100] of Byte;
begin
  Result := '';
  SS := TStringStream.Create;
  HexToStr(Value,SS);
  DS := TStringStream.Create('',TEncoding.ANSI);
  try
    try
    SS.Position:=0;
    SS.ReadBuffer(Size, SizeOf(Size));
    //key转成Ansi
    for i := 1 to  Length(Key) do KeyBuffer[i - 1]:=  Ord(Key[i]);
    {  --  128 位密匙最大长度为 16 个字符 --  }
    if KeyBit = kb128 then
    begin
      FillChar(AESKey128, SizeOf(AESKey128), 0 );
      Move(KeyBuffer, AESKey128, Min(SizeOf(AESKey128), Length(Key)));
    // for I := 0 to Length(Key) - 1 do Write(Format('0x%X,',[AESKey128[i]]));
      DecryptAESStreamECB(SS, SS.Size - SS.Position , AESKey128, DS);
    end;
    {  --  192 位密匙最大长度为 24 个字符 --  }
    if KeyBit = kb192 then
    begin
      FillChar(AESKey192, SizeOf(AESKey192), 0 );
      Move(KeyBuffer, AESKey192, Min(SizeOf(AESKey192), Length(Key)));
      DecryptAESStreamECB(SS, SS.Size - SS.Position, AESKey192, DS);
    end;
    {  --  256 位密匙最大长度为 32 个字符 --  }
    if KeyBit = kb256 then
    begin
      FillChar(AESKey256, SizeOf(AESKey256), 0 );
      Move(KeyBuffer, AESKey256, Min(SizeOf(AESKey256), Length(Key)));
      DecryptAESStreamECB(SS, SS.Size - SS.Position, AESKey256, DS);
    end;
    Result:=Ds.DataString;
    except
      Writeln('DecryptString Error');
    end;
  finally
    SS.Free;
    DS.Free;
  end;
end;

end.
