unit uEncrypt;

interface
uses
 {$IFDEF WIN32}
  Winapi.Windows,
 {$ENDIF}
  System.SysUtils,
  System.Classes,
  AES,DES,uDes2;

const
  C_Name_KeyTable_Key = '!d&b9HJK&%)bceak';  //key表用的解密key
  C_Name_Key_File = '/NameKeyTable.txt';        //Select * From HardCard表 内提取
  V24KEYBASE = 'Yka7ghiUlmBjbc9XdfnKoW5wpqr6tIuv8xyz4+/FG0HJL3MNOPQeRsS2TVZAC1DE';

  C_CHECK_CHR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#+{}[]^"%.<>\'#$F;

type
  TBornData = class
    private
      BornBit:Array [0..7] of Byte;
      function GetDay: Cardinal;
      function GetMonth: Cardinal;
      function GetYear: Cardinal;
    function GetTurnBack: String;
    function GetYearSum: Cardinal;
    public
      constructor Create(_BornStr:String);

      property TurnBack:String read GetTurnBack;
      property Year:Cardinal read GetYear;
      property Month:Cardinal read GetMonth;
      property Day:Cardinal read GetDay;
      property YearSum:Cardinal read GetYearSum;
  end;

  TF11Data = class
    public
      Key1:String;
      Key2:string;
      constructor Create(_F11Str:String);
  end;

  TSexData = class
    private
      mSex:Cardinal;
      function GetMark: String;
    public
      Key:array [0..3] of integer;
      constructor Create(_Sex:Integer;_Born:String);

      property Mark:String  read GetMark;
      property Num:Cardinal read mSex;
  end;

  TPhoneNumberEncryt = class
  private
    function MakeKeyBase(Src: String; _EDX, _ECX, _ESI: Integer): string;
    function MakeDESKey(_Key:String):string;
    function CheckChar(n: Byte): Char;
  public
    constructor Create();

    Function EncryptData(NumData:string):String;
    Function DecryptData(NumData:string):String;
  end;

  TJzNameMgr = class
    private
      mActive:Boolean;
      Keys:TStringList;
      //key表初始化
      Function LoadKeyTable():Boolean;
      function GetKeyCount: Cardinal;
      function MakeKey(_Key:Pchar;_n:Integer):String;
    public
      constructor Create();

      //解密名字
      Function DecodeJzName(_Bron,_Name:Pchar):String;
      Function EncodeJzName(_Bron,_Name:Pchar):String;

      property KeyTableCount:Cardinal read GetKeyCount;
      property Active:Boolean read mActive;
  end;

    TJM_CHILD_DATA = class
     private
       mb_Year,mb_Month,mb_Day:Integer;
       mb_Day_h,mb_Day_l,mb_Month_h,mb_Month_l:Integer;

       mSex:Integer;
       mYearSum:Integer;         //年份的4个数字累加

       mField11:String;
       mBorn:String;
       mShortBorn:String;        //去掉分隔符的日期
       mShortBornTurn:String;    //日期倒叙
       mKeyBase:String;

       function MakeKeyBase:String;
       function DateTurnBack(S: String): String;
       function DecryptJZ(_JzData:string;DecodeIndex: Integer): String;
       function getDateKey: String;
       function getYearKey(A60: Integer): String;
       function getPublicKey(A58, A5C, A60, _ESI, _EDI, C8, CC, D0: Integer): String;
       {新版加密解密}
       function GetModeHash(InKey:Pchar;Count,BornSum:Integer): Integer;
       function MakeCoreKey(Src: String; Month, DayMonth, Year: Cardinal): String;
       function MakeF11(BaseKey:String):String;

       function WriteAnsiChar(Src:string;C:Cardinal):String;
       function WriteDword(Src: string; dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi,
      dw_0xC8, dw_0xCC, dw_0xD0: Cardinal): String;
       //旧版秘钥 生成
       Function CalcKey(_JzData:string;DecodeIndex:Integer):String;
     public
        {旧版加密解密}
       Function EncryptData(_JzData:string;Index:Cardinal):String;
       Function DecryptData(_JzData:string;pIndex:PCardinal):String;
        {新版加密解密}
       Function NewEncryptData(_JzData:string):String;
       Function NewDecryptData(_JzData:string):String;

       // 生日，性别，接种疫苗数据，字段11数据
       constructor Create(_Born,_Sex,_Field11:String);
  end;

var
  g_PhoneNum:TPhoneNumberEncryt;
  g_JzNameMgr:TJzNameMgr;
  g_JzData:TJM_CHILD_DATA;
  C_CHECK_ARR:array [0..66] of BYTE= (
  $4A,$2F,$5C,$45,$57,$29,$11,$42,$0F,$1B,
  $7C,$2D,$52,$25,$26,$22,$10,$1A,$28,$53,
  $56,$7B,$58,$40,$4D,$2A,$2B,$2C,$19,$3A,
  $3D,$7D,$14,$12,$49,$21,$4F,$23,$5D,$0E,
  $55,$17,$5A,$18,$44,$4E,$4B,$4C,$48,$24,
  $16,$5B,$43,$41,$47,$50,$5F,$3B,$13,$3E,
  $59,$46,$51,$08,$5E,$2E,$3C
  );
function PhoneNumberEncrypt(_WorkMode,_Version:Integer;_SrcCode,_Key:PChar;_OutBuffer:PChar):Integer;
function ChildNameEncrypt(_WorkMode:Integer;_Born,_Code:PChar;_OutBuffer:PChar):Integer;
Function JzDataEncrypt(_WorkMode,_Version:Integer;_Born,_Sex,_JzData,_Field11:PChar;OutBuffer:Pchar;pIndex:PCardinal):Integer;


Procedure Dbgprint(Str:String;Args:Array of const);

implementation

Procedure Dbgprint(Str:String;Args:Array of const);
begin
 {$IFDEF WIN32}
  OutputDebugString(PChar(Format(Str,Args)));
 {$ENDIF}
end;


constructor TPhoneNumberEncryt.Create;
begin
end;

function TPhoneNumberEncryt.CheckChar(n:Byte):Char;
begin
  Result:=#0;
  if (n > $2F) and (n < $3A) then  Result:=Char(n);
  if n < 10 then Result:= Char(n + $30);
end;


function TPhoneNumberEncryt.DecryptData(NumData: string): String;
var
  a,b,c,d,i:Integer;
  Len,Tmp,p:integer;
  TmpStr:String;
begin
  Result:='';
  Len:=Length(NumData);
  if Len > 0 then
    begin
      if NumData[Len] = '=' then
        begin
          P:=Len;
          i:=1;
          TmpStr:='';
          while p <> 0 do
           begin
           //V24KEYBASE = 'Yka7ghiUlmBjbc9XdfnKoW5wpqr6tIuv8xyz4+/FG0HJL3MNOPQeRsS2TVZAC1DE';
             a:=Pos(NumData[i],V24KEYBASE) - 1;
             b:=Pos(NumData[i+1],V24KEYBASE) - 1;
             c:=Pos(NumData[i+2],V24KEYBASE) - 1;
             d:=Pos(NumData[i+3],V24KEYBASE) - 1;
           //  Writeln(Format('a:%d b:%d c:%d d:%d',[a,b,c,d]));
             Tmp := (a shl 2 and $FC) or (b shr 4 and 3);

             TmpStr :=TmpStr + CheckChar(Tmp);

           //  Writeln(Format('Index:%d ->[0x%X] %s',[i,Tmp, CheckChar(Tmp)]));
             Tmp:=(b shl 4 and $0f) or (c shr 2 and $0f);
             TmpStr :=TmpStr + CheckChar(Tmp);
           //  Writeln(Format('Index:%d ->[0x%X] %s',[i,Tmp, CheckChar(Tmp)]));
             Tmp:=(c shl 6 and $c0) or d;
             TmpStr :=TmpStr + CheckChar(Tmp);
           //  Writeln(Format('Index:%d ->[0x%X] %s',[i,Tmp, CheckChar(Tmp)]));
             i := i + 4;
             Dec(p);
           end;
           Result:=TmpStr;

        end;
    end;


end;

function TPhoneNumberEncryt.EncryptData(NumData: string): String;
var
  a,b,c,d,i,j,k:Integer;
  TmpStr:String;
  n:Integer;
begin
 TmpStr:='';
 k:=1;
 for i := 1 to Length(NumData) do
   begin
     b:=0;
     c:=0;
     for j := 0 to 2 do
       begin
         if k <= Length(NumData) then
           begin
             n:=  Ord(NumData[k]);
             //Writeln(Format('--->0x%X',[n]));
             case j of
               0:begin
                   a:=(n - $30) shr 2;
                   b:=((n - $30) and 3) shl 4;
                   TmpStr :=TmpStr + V24KEYBASE[a + 1];
                   TmpStr :=TmpStr + V24KEYBASE[b + 1];
                 end;
               1:begin
                   c:= (b shl 4 and $0f) or (n - $30) and $F shl 2;
                   TmpStr :=TmpStr + V24KEYBASE[c + 1];
                 end;
               2:begin
                   d:= (c shl 6 and $c0) or n;
                   TmpStr :=TmpStr + V24KEYBASE[d + 1];
                 end;
             end;
             //Writeln(Format('a=%d, b=%d, c=%d, d=%d',[a,b,c,d]));
             Inc(k);
           end;
       end;


   end;
// Writeln(TmpStr);

 Result:=TmpStr + '=';
end;

function TPhoneNumberEncryt.MakeDESKey(_Key: String): string;
var
  KeyLen,I:Integer;
  TmpStr:String;
  Head,Tail,TmpInt:Integer;
  Keys:Array of Byte;
  KeyBase:String;
  FinalKey:array [0..7] of Char;
begin
  KeyLen:=Length(_Key);
  TmpStr:=Copy(_Key,4,KeyLen - 3);
  TmpStr:=Copy(TmpStr,1,Length(TmpStr) - 3);

  KeyLen:=Length(TmpStr);

  Head:= Ord(TmpStr[1]);// Keys[0];
  Tail:=Ord(TmpStr[KeyLen]);//Keys[KeyLen-1];
  TmpStr:=Copy(TmpStr,2,KeyLen - 2);
  KeyBase:=MakeKeyBase(TmpStr,Head,Tail + Head,Tail);
  //获得基础秘钥后的处理
  KeyLen:=Length(KeyBase);
  SetLength(Keys,KeyLen);
  for i := 0 to KeyLen - 1 do Keys[i]:=Ord(KeyBase[i+1]);
  for i := 0 to 7 do  FinalKey[i] :=#0;

  if Tail mod 3 = 0 then
    begin
      FinalKey[0]:=Chr(Keys[0]);
      FinalKey[1]:=Chr(Keys[5]);
    end
  else
   begin
      FinalKey[0]:=Chr(Keys[1]);
      FinalKey[1]:=Chr(Keys[4]);
   end;

  //加在上面的后面esi
  FinalKey[2]:= Chr(Tail);
  TmpInt:= Keys[4];
  if TmpInt mod 2 = 0 then
    begin
      FinalKey[3] := Chr((Keys[0] + Keys[5]) DIV 2);
      FinalKey[4] := Chr((Keys[4] + Keys[1]) DIV 2);
      FinalKey[5] := Chr((Keys[0] + Keys[2] + Keys[3] + Keys[4]) DIV 4);
      FinalKey[6] := Chr((Keys[0] + Keys[1] + Keys[2]) div 3);
    end
  else
    begin
      FinalKey[3] := Chr((Keys[0] + Keys[2]) DIV 2);
      FinalKey[4] := Chr((Keys[5] + Keys[1]) DIV 2);
      FinalKey[5] := Chr((Keys[2] + Keys[1] + Keys[4] + Keys[5]) DIV 4);
      FinalKey[6] := Chr((Keys[3] + Keys[4] + Keys[5]) div 3);
    end;

  Result:= FinalKey;
end;

function TPhoneNumberEncryt.MakeKeyBase(Src: String; _EDX, _ECX, _ESI: Integer): string;
var
  Len,i:Integer;
  Tmp:Integer;
  A:Byte;
  Seed,Seed2:Integer;
begin
  //Dbgprint('n = %X,o = %X,m = %X',[_EDX, _ECX, _ESI]);
  Seed:=_EDX;
  Seed2:=_ECX;
  Len:=Length(Src);
  if (Len div 2) > 0 then
    begin
      for i:=0 to Len div 2 - 1 do
        begin
          Tmp:= Strtoint('$' + Copy(Src,i + i + 1,2));
          A:= Seed shr 8;
          Result:= Result + Chr(Tmp xor A);
          Seed:= (Tmp + Seed) * Seed2 + _ESI;
        end;
    end;
end;




constructor TJzNameMgr.Create;
begin
  Keys:=TStringList.Create;
  mActive:= LoadKeyTable();
end;

function TJzNameMgr.DecodeJzName(_Bron, _Name: Pchar): String;
var
  n,a:Integer;
  SubKey:String;
  ResName:PChar;
begin
  Result:= _Name;
  if Length(_Bron) = 10 then
    begin
      //生日取最后个数字
      a:= Ord(_Bron[9]) - $30;
      //头保存2个字节
      for n := 0 to Keys.Count -1 do
        begin
          //计算名字解密的key
          SubKey:=MakeKey(Pchar(Keys[n]),a);
          //使用256的AES
          ResName:=PChar(DecryptString(_Name,SubKey,kb256));
          //Writeln(ResName);
          //判断标志位 是否解密成功
          if (ResName[0] = 'j') and (ResName[Length(ResName) - 1] = 'x') then
            begin
              //名字修正 补头 掐尾
              ResName[Length(ResName)-1]:=#0;
              Result:=PChar(@ResName[1]);
              Break;
            end;
        end;
    end;
end;

function TJzNameMgr.EncodeJzName(_Bron, _Name: Pchar): string;
var
  a:Integer;
  SubKey:String;
  NameHead:String;
  NameBody:Pchar;
  NameCode:String;
begin
  Result:= _Name;
  if Length(_Bron) = 10 then
    begin
      //生日取最后个数字
      a:= Ord(_Bron[9]) - $30;
      NameHead:=_Name[0];
      NameBody:=@_Name[1];
      NameCode:='j' + NameBody + 'x';
      //直接使用第一个KEY
      SubKey:=MakeKey(Pchar(Keys[0]),a);
      //Writeln(Format('Key = %s',[SubKey]));
      Result:=Pchar(NameHead + EncryptString(NameCode,SubKey,kb256));
    end;
end;

function TJzNameMgr.GetKeyCount: Cardinal;
begin
  Result:= Keys.Count;
end;

function TJzNameMgr.LoadKeyTable: Boolean;
var
  Fs:TStringList;
  i:Integer;
  FileName:String;
begin
  Result:=False;
  FileName:=GetCurrentDir() + C_Name_Key_File;
  Writeln(Format('Load KeyTable [%s]',[FileName]));
  if FileExists(FileName) then
    begin
      Fs:=TStringList.Create;
      try
        Fs.LoadFromFile(FileName);
        //Writeln(Format('Load KeyTable Count = %d',[Fs.Count]));
        if Fs.Count > 0  then
          begin

            for i := 0 to Fs.Count - 1 do
              begin
                Keys.Add(DecryptString(Fs[i],C_Name_KeyTable_Key));   //使用 128的AES
              end;
          end;
       // Keys.Sort;
        Result:= Fs.Count = Keys.Count;
      finally
        Fs.Free;
      end;
   end
  else
   begin
     Writeln('KeyTableFile Not Find!');
   end;
  Writeln(Format('KeyTable Line Count:%d',[Keys.Count]));
end;

function TJzNameMgr.MakeKey(_Key: Pchar;_n:Integer): String;
begin
  Result:= _Key[2] + '\' + _Key[6] + 'j' + _Key[$19] + 'x' + _Key[_n + 9];
end;

function IsDoubleNum(n:string):boolean;
begin
  Result:= (StrToInt(n) mod 2) = 0;
end;

Function QuickJzDataEncrypt(_WorkMode:Integer;JzData,Born:Pchar):String;
var
  DataKey:array [0..99] of Byte;

  Res:TStringList;
  DataStrKey:String;
  DataLen,i,KeyIdx:integer;
  Rb,saltByte,m:Integer;
begin
  Result:='';
  Res:=TStringList.Create;
  try
    if Born <> nil then
      begin
        if ExtractStrings(['-'],[],Born,Res) > 0 then
          begin
            //Writeln(Format('Res:%d',[Res.Count]));
            DataStrKey:= Res[2] + Res[1] + Res[0];
            if IsDoubleNum(Res[1]) and IsDoubleNum(Res[2]) then DataStrKey:=Res[0] + Res[1] + Res[2];
            if IsDoubleNum(Res[1]) and Not(IsDoubleNum(Res[2])) then DataStrKey:=Res[0] + Res[2] + Res[1];
            if Not(IsDoubleNum(Res[1])) and IsDoubleNum(Res[2]) then DataStrKey:=Res[1] + Res[0] + Res[2];


            DataLen:=Length(DataStrKey);
           // Writeln(Format('DataKey:%s Len:%d',[DataStrKey,DataLen]));
            for i := 0 to DataLen - 1 do
              begin
                DataKey[i]:= ord(DataStrKey[i+1]);
              end;
            //解密
            if _WorkMode = 0 then
              begin
                saltByte:= StrToInt('$' + JzData[0] + JzData[1]) and $FF;
                KeyIdx:=0;
                for i := 1 to (Strlen(JzData) div 2) - 1 do
                  begin
                    if KeyIdx >= DataLen then  KeyIdx:= 0;
                    m:= StrToInt('$' + JzData[i*2] + JzData[i*2+1]);
                    Rb:= (m and $FF) xor (DataKey[KeyIdx] and $FF);Inc(KeyIdx);
                    if Rb <= saltByte then Rb:=Rb + $FF;
                    Rb:=Rb - saltByte;
                    Result:= Result + Char(Rb);
                    saltByte:= m and $FF;
                  end;
              end
            else
              begin
                KeyIdx:=0;
                saltByte := Random(255);
                for i := 0 to Strlen(JzData) do
                  begin
                    if KeyIdx >= DataLen then  KeyIdx:= 0;
                    m:=ord(JzData[i]);
                   // Writeln(Format('%X ->%X',[m,saltByte]));
                    Result:=Result+inttohex(saltByte,2);
                    Rb:=saltByte + ord(JzData[i]);
                    if Rb > $FF then  Rb:=Rb - $FF;
                    saltByte:= Rb xor (DataKey[KeyIdx] and $FF);Inc(KeyIdx);//Dec(KeyIdx);
                  end;
              end;
          end;
      end;
     // Result:=Widestring(R) ;
  finally
    Res.Free;
  end;
end;



Function JzDataEncrypt(_WorkMode,_Version:Integer;_Born,_Sex,_JzData,_Field11:PChar;OutBuffer:Pchar;pIndex:PCardinal):Integer;
var
  Jz:TJM_CHILD_DATA;
  Res,Temp:String;
  F11Len,j:Integer;
begin
   //这里判断个 F11 = 1的情况，是最老的一个版本
   F11Len := Length(_Field11);
   //writeln(Format('F11 Length = %d',[F11Len]));
   try
     if F11Len < 10 then
       begin
         //Writeln(Format('JzData:%s',[_JzData]));
         Temp:=QuickJzDataEncrypt(_WorkMode,_JzData,_Born);
         Res:= Format('\x%X',[Ord(Temp[1])]);
         for j := 2 to length(PChar(Temp)) do
            begin
              if Ord(Temp[j]) = 0 then break;
              Res:= Format('%s\x%X',[Res,Ord(Temp[j])]);
            end;
        // Writeln(Format('Result = %s',[Res]));
       end
     else
       begin
         Jz:=TJM_CHILD_DATA.Create(_Born,_Sex,_Field11);
         try
          // Writeln(Format('WorkMode = %d Version = %d',[_WorkMode,_Version]));
           if _Version = 1 then
             begin
               if _WorkMode = 0 then Res:=Jz.DecryptData(_JzData,pIndex);
               if _WorkMode = 1 then Res:=Jz.EncryptData(_JzData,pIndex^);
             end;
           if _Version = 2 then
             begin
               if _WorkMode = 0 then Res:=Jz.NewDecryptData(_JzData);
               if _WorkMode = 1 then Res:=Jz.NewEncryptData(_JzData);
             end;
          //Dbgprint('Res Size1:%d',[lstrlen(Pchar(Res))]);
         finally
           Jz.Free;
         end;
       end;

      {$IFDEF WIN32}
        MoveMemory(OutBuffer,PChar(Res),lstrlen(Pchar(Res)) * 2);
        Result:=lstrlen(OutBuffer);
      {$ELSE}
        StrCopy(OutBuffer,PChar(Res));
        Result:=Length(OutBuffer);
      {$ENDIF}
   except
     Result:=0;
   end;
end;

constructor TJM_CHILD_DATA.Create(_Born, _Sex,  _Field11: String);
var
  DateValue:TStringList;
begin
  mSex:=StrToInt(_Sex);
  DateValue:=TStringList.Create;
  try
    try
      DateValue.Delimiter:='-';
      DateValue.DelimitedText:=_Born;
      if DateValue.Count = 3 then
        begin
          mBorn:=_Born;
          mField11:=_Field11;
          mb_Year:= StrToInt(DateValue[0]);
          mb_Month:= StrToInt(DateValue[1]);
          mb_Day:= StrToInt(DateValue[2]);
          mb_Day_h:=StrToInt(Copy(DateValue[2],1,1));
          mb_Day_l:=StrToInt(Copy(DateValue[2],2,1));
          mb_Month_h:=StrToInt(Copy(DateValue[1],1,1));
          mb_Month_l:=StrToInt(Copy(DateValue[1],2,1));
          mShortBorn:= DateValue[0] + DateValue[1] + DateValue[2];
          mShortBornTurn:=DateTurnBack(mShortBorn);
          mYearSum:= StrToInt(Copy(DateValue[0],1,1)) + StrToInt(Copy(DateValue[0],2,1)) +
                     StrToInt(Copy(DateValue[0],3,1)) + StrToInt(Copy(DateValue[0],4,1));
          mKeyBase:=MakeKeyBase();
        end;
    except
      //MessageBox(0,'创建对象错误！','错误',0);
    end;
  finally
    DateValue.Free;
  end;
end;

Function TJM_CHILD_DATA.DateTurnBack(S:String):String;
var
  Len,I:Integer;
begin
  Len:=Length(S);
  if Len > 1 then
   begin
     Result:='';
     for I := Len downto 1 do
       Result:=Result + S[i];
   end;

end;

function TJM_CHILD_DATA.getDateKey():String;
var
  TmpStr:String;
begin
  if mSex = 1 then
    begin
      TmpStr:=Copy(mShortBorn,5,4);
    end
  else
    begin
      TmpStr:=Copy(mShortBornTurn,5,4);
    end;
  Result:=Char(StrToInt(Copy(TmpStr,1,2)) + $1E) + Char(StrToInt(Copy(TmpStr,3,2)) + $1E);
end;

function TJM_CHILD_DATA.getYearKey(A60:Integer):String;
begin
  if (mYearSum + A60) < $28 then
    Result:=Char(mYearSum + A60 + $28)
  else
    Result:=Char(mYearSum + A60);
end;

function TJM_CHILD_DATA.getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0:Integer):String;
begin
 try
  if A58 mod 2 - 1 = 0 then
    begin
      if _EDI mod 2 - 1 = 0 then
        Result:=Char(CC + _EDI) + IntToStr(C8)
      else
        Result:=Char(C8 + _EDI) + IntToStr(CC);

      if A60 mod 2 = 0 then
        Result:=Result + Char(D0 + A60) + IntToStr(_ESI)
      else
        Result:=Result + IntToStr(_ESI) + Char(D0 + A5C);
    end
  else
    begin
      if _EDI mod 3 - 1= 0 then
        Result:=IntToStr(D0) + Char(_EDI + _ESI)
      else
        Result:=Char(_EDI + _ESI) + IntToStr(D0);

      if A5C mod 2 = 0 then
        Result:=Result + Char(CC + A5C) + IntToStr(C8)
      else
        Result:=Result + IntToStr(C8) + Char(CC + A5C);
    end;
 except
   Dbgprint('getPublicKey Error',[]);
   Result:='0';
 end;
end;

function CheckDecryptData(Rep:Pchar):Boolean;
var
  i,j,L:Integer;
begin
  Result:=True;
  L:=Length(Rep);
  //Writeln(Format('LEN:%d -->%s',[L,Rep]));
  if L > 1 then
    begin
      for i := 0 to L div 2 - 1 do
        begin
          if Ord(Rep[i * 2]) > 255 then
            Begin
              Result:=False;
              Break;
            End;
          if  ((Rep[i * 2 + 1] >= '0') and (Rep[i * 2 + 1] <= '9')) and
              ((Rep[i * 2] < '0') or (Rep[i * 2] > '9')) then
            begin
             // Writeln(Format('pos %d %s',[i,Rep[i * 2 + 1]]));
              Result:=True;
            end
          else
            begin
              Result:=False;
              //2019-6-17 添加意外，会存在6的类型
              //A1C1C2C3S1S2S3123123"1"2"3%1%2%3616263=1\1\2\3
              if (Rep[i * 2] = '6') then Result:=True;
              //2019-6-17 添加意外，会存在6的类型
              //81
              if (Rep[i * 2] = '8') then Result:=True;

              Break;
            end;
        end;
    end
  else
    Result:=False;
end;

function TJM_CHILD_DATA.DecryptData(_JzData: string;pIndex:PCardinal): String;
var
  i,j,m,n:Integer;
  Rep:String;
begin
  for i := 0 to 4 do
    begin
      Rep:=DecryptJZ(_JzData,i);
      if Rep = 'E' then
        begin
          Break;
        end;
      //判断解出来的值是否合法

      try
      OutputDebugString(PChar(Rep));
      if CheckDecryptData(Pchar(Rep)) then
        begin
          Writeln('+++++' + Rep);
          Dbgprint(Rep,[]);
        //  Dbgprint('SIZE:%d',[lstrlen(PChar(Rep))]);
          if Assigned(pIndex) then pIndex^:= i;
          Result:= Format('\x%X',[Ord(Rep[1])]);
          for j := 2 to length(PChar(Rep)) do
            begin
              if Ord(Rep[j]) = 0 then break;
              Result:= Format('%s\x%X',[Result,Ord(Rep[j])]);
            end;
         Break;
        end;
      except
        Writeln('Error LOOP');
        Continue;
      end;
    end;
  //Dbgprint(Result,[]);
  if I = 5 then Result:='DecryptFaild';
end;


Function TJM_CHILD_DATA.CalcKey(_JzData:string;DecodeIndex:Integer):String;
var
  D0,C8,CC,_ESI:Integer;
  A58,A5C,A60,A64,A68,_EDI:Integer;
  I:Integer;
  Keys:Array of Byte;
begin
  try
  //基础秘钥转16进制保存
  SetLength(Keys,Length(mKeyBase));
  for i := 0 to Length(mKeyBase) - 1 do Keys[i]:=Ord(mKeyBase[i+1]);

  Result:='';
  _ESI:= mb_Day_h;
  C8:= mb_Month_h;
  CC:= mb_Month_l;
  D0:= mb_Day_l;

  _EDI:=Keys[5] - $F;
  A58:= Keys[1] - 7;
  A5C:=Keys[8] - _ESI - D0;
  A60:=Keys[10] + _ESI + C8;
  A64:=Keys[4] - _ESI - C8;
  A68:=Keys[2] + 3;
  case DecodeIndex of
    1:begin
        Result:= Char(mSex + _ESI + A58);
        Result:= Result + Char(_EDI + D0 + mSex);
        if mSex = 2 then
          Result:=Result + Char(A68) + Char(A58)
        else
          Result:=Result + Char(A60) + Char(A5C);
        //
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A58 + A60) DIV 2);
      end;
    2:begin
        Result:= Char(mSex + D0 + A58);
        Result:=Result + Char(mSex + D0 + _EDI);
        if mSex = 2 then
          Result:=Result + Char(A64) + Char(A58)
        else
          Result:=Result + Char(A60) + Char(_EDI);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A58 + A5C) DIV 2);
      end;
    3:begin
        Result:=Char(mSex + _ESI + _EDI);
        Result:=Result + Char(mSex + D0 + _EDI);
        if mSex = 2 then
          Result:=Result + Char(A68) + Char(_EDI)
        else
          Result:=Result + Char(A60) + Char(A68);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A60 + _EDI) DIV 2);
      end;
    4:begin
        Result:=Char(mSex + D0 + _EDI);
        Result:= Result + Result;
        if mSex = 2 then
          Result:=Result + Char(A58) + Char(A5C)
        else
          Result:=Result + Char(_EDI) + Char(A60);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A60 + _EDI) DIV 2);
      end;
  else
      begin
        Result:=Char(mSex + _ESI + A58);
        Result:= Result + Char(mSex + D0 + _EDI);
        if mSex = 2 then
          Result:=Result + Char(A68) + Char(A58)
        else
          Result:=Result + Char(A60) + Char(A5C);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A58 + A60) DIV 2);
      end;
  end;
  Result:=Result + getYearKey(A60);

  Result:= Copy(Result,mb_Month,1) + Copy(Result,1,mb_Month-1) + Copy(Result, mb_Month + 1,Length(Result) - mb_Month);

  if mSex = 1 then
    Result:=Char(A64 + _ESI) + Result + Char(A68 + D0)
  else
    Result:=Char(A68 + _ESI) + Result + Char(A64 + D0);

  if A68 mod 2 - 1 = 0 then
    begin
      Result:= Result + Char((A58 + A68 + _ESI) DIV 2);
      //key 0013
      Result:= Result + Char((A64 + _EDI) DIV 2);
      //key 0014
      Result:= Result + Char((A58 + A5C + A60  + A64 + _ESI) div 4);
      //key 0015
      Result:= Result + Char((A58 + _EDI + A5C) div 3);
    end
  else
    begin
      Result:= Result + Char((A58 + A5C) DIV 2);
      //key 0013
      Result:= Result + Char((A68 + _EDI + _ESI) DIV 2);
      //key 0014
      Result:= Result + Char((A5C + _EDI  + A64 + A68) div 4);
      //key 0015
      Result:= Result + Char((A60 + A64 + A68 + _ESI) div 3);
    end;
  except
    Result:='ErrorKey';
    Writeln('CaclKey Error');
  end;
end;

Function TJM_CHILD_DATA.DecryptJZ(_JzData:string;DecodeIndex:Integer):String;
var
  D0,C8,CC,_ESI:Integer;
  A58,A5C,A60,A64,A68,_EDI:Integer;
  I:Integer;
  Keys:Array of Byte;
begin
  try
  //基础秘钥转16进制保存
  SetLength(Keys,Length(mKeyBase));
  for i := 0 to Length(mKeyBase) - 1 do Keys[i]:=Ord(mKeyBase[i+1]);

  Result:='';
  _ESI:= mb_Day_h;
  C8:= mb_Month_h;
  CC:= mb_Month_l;
  D0:= mb_Day_l;

  _EDI:=Keys[5] - $F;
  A58:= Keys[1] - 7;
  A5C:=Keys[8] - _ESI - D0;
  A60:=Keys[10] + _ESI + C8;
  A64:=Keys[4] - _ESI - C8;
  A68:=Keys[2] + 3;
  case DecodeIndex of
    1:begin
        Result:= Char(mSex + _ESI + A58);
        Result:= Result + Char(_EDI + D0 + mSex);
        if mSex = 2 then
          Result:=Result + Char(A68) + Char(A58)
        else
          Result:=Result + Char(A60) + Char(A5C);
        //
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A58 + A60) DIV 2);
      end;
    2:begin
        Result:= Char(mSex + D0 + A58);
        Result:=Result + Char(mSex + D0 + _EDI);
        if mSex = 2 then
          Result:=Result + Char(A64) + Char(A58)
        else
          Result:=Result + Char(A60) + Char(_EDI);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A58 + A5C) DIV 2);
      end;
    3:begin
        Result:=Char(mSex + _ESI + _EDI);
        Result:=Result + Char(mSex + D0 + _EDI);
        if mSex = 2 then
          Result:=Result + Char(A68) + Char(_EDI)
        else
          Result:=Result + Char(A60) + Char(A68);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A60 + _EDI) DIV 2);
      end;
    4:begin
        Result:=Char(mSex + D0 + _EDI);
        Result:= Result + Result;
        if mSex = 2 then
          Result:=Result + Char(A58) + Char(A5C)
        else
          Result:=Result + Char(_EDI) + Char(A60);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A60 + _EDI) DIV 2);
      end;
  else
      begin
        Result:=Char(mSex + _ESI + A58);
        Result:= Result + Char(mSex + D0 + _EDI);
        if mSex = 2 then
          Result:=Result + Char(A68) + Char(A58)
        else
          Result:=Result + Char(A60) + Char(A5C);
        Result:=Result + getDateKey();
        Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
        //!!
        Result:=Result + Char((A58 + A60) DIV 2);
      end;
  end;
  Result:=Result + getYearKey(A60);

  Result:= Copy(Result,mb_Month,1) + Copy(Result,1,mb_Month-1) + Copy(Result, mb_Month + 1,Length(Result) - mb_Month);

  if mSex = 1 then
    Result:=Char(A64 + _ESI) + Result + Char(A68 + D0)
  else
    Result:=Char(A68 + _ESI) + Result + Char(A64 + D0);

  if A68 mod 2 - 1 = 0 then
    begin
      Result:= Result + Char((A58 + A68 + _ESI) DIV 2);
      //key 0013
      Result:= Result + Char((A64 + _EDI) DIV 2);
      //key 0014
      Result:= Result + Char((A58 + A5C + A60  + A64 + _ESI) div 4);
      //key 0015
      Result:= Result + Char((A58 + _EDI + A5C) div 3);
    end
  else
    begin
      Result:= Result + Char((A58 + A5C) DIV 2);
      //key 0013
      Result:= Result + Char((A68 + _EDI + _ESI) DIV 2);
      //key 0014
      Result:= Result + Char((A5C + _EDI  + A64 + A68) div 4);
      //key 0015
      Result:= Result + Char((A60 + A64 + A68 + _ESI) div 3);
    end;


  //Writeln(Format('Key = %s',[Result]));
  Result:=DecryptString(_JZData,Result,kb256);
  except
    Result:='DE';
  end;
end;


function TJM_CHILD_DATA.EncryptData(_JzData: string;Index:Cardinal): String;
var
  D0,C8,CC,_ESI:Integer;
  A58,A5C,A60,A64,A68,_EDI:Integer;
  I:Integer;
  Keys:Array of Byte;
  Src:TStringList;
  Code:String;

  Base64Key:string;
begin
  Base64Key:=CalcKey(_JzData,Index);
  if Base64Key <> 'ErrorKey' then
    begin
      Writeln('Src:' + _JzData);
      Writeln('Key:' + Base64Key);

      Result:=EncryptString(_JzData,Base64Key,kb256);
    end
  else
    begin
      Result:='Encrypt Error';
    end;

  Exit;

  Code:=_JzData;//'';
  try
  //基础秘钥转16进制保存
  SetLength(Keys,Length(mKeyBase));
  for i := 0 to Length(mKeyBase) - 1 do Keys[i]:=Ord(mKeyBase[i+1]);

  Result:='';
  _ESI:= mb_Day_h;
  C8:= mb_Month_h;
  CC:= mb_Month_l;
  D0:= mb_Day_l;

  _EDI:=Keys[5] - $F;
  A58:= Keys[1] - 7;
  A5C:=Keys[8] - _ESI - D0;
  A60:=Keys[10] + _ESI + C8;
  A64:=Keys[4] - _ESI - C8;
  A68:=Keys[2] + 3;

  Result:= Char(mSex + _ESI + A58);
  Result:= Result + Char(_EDI + D0 + mSex);
  if mSex = 2 then
    Result:=Result + Char(A68) + Char(A58)
  else
    Result:=Result + Char(A60) + Char(A5C);
  //
  Result:=Result + getDateKey();
  Result:=Result + getPublicKey(A58,A5C,A60,_ESI,_EDI,C8,CC,D0);
  //!!
  Result:=Result + Char((A58 + A60) DIV 2);

  Result:=Result + getYearKey(A60);

  Result:= Copy(Result,mb_Month,1) + Copy(Result,1,mb_Month-1) + Copy(Result, mb_Month + 1,Length(Result) - mb_Month);

  if mSex = 1 then
    Result:=Char(A64 + _ESI) + Result + Char(A68 + D0)
  else
    Result:=Char(A68 + _ESI) + Result + Char(A64 + D0);

  if A68 mod 2 - 1 = 0 then
    begin
      Result:= Result + Char((A58 + A68 + _ESI) DIV 2);
      //key 0013
      Result:= Result + Char((A64 + _EDI) DIV 2);
      //key 0014
      Result:= Result + Char((A58 + A5C + A60  + A64 + _ESI) div 4);
      //key 0015
      Result:= Result + Char((A58 + _EDI + A5C) div 3);
    end
  else
    begin
      Result:= Result + Char((A58 + A5C) DIV 2);
      //key 0013
      Result:= Result + Char((A68 + _EDI + _ESI) DIV 2);
      //key 0014
      Result:= Result + Char((A5C + _EDI  + A64 + A68) div 4);
      //key 0015
      Result:= Result + Char((A60 + A64 + A68 + _ESI) div 3);
    end;
  Writeln(Format('Code:%s Key:%s',[Code,Result]));
  Result:=EncryptString(Code,Result,kb256);
  except
    Result:='DE';
  end;
end;

function TJM_CHILD_DATA.MakeKeyBase: String;
var
  Seed:Integer;
  TmpBit,TmpHex:Byte;
  NewKey:String;
  NewKeyLen,i:Integer;
begin
  NewKey:=Copy(mField11,1,3) + Copy(mField11,$C,Length(mField11)-8);
  NewKey:=Copy(NewKey,1,Length(NewKey) - 8);
  NewKeyLen:=Length(NewKey);
  //种子
  Seed:=mb_Month;
  try
    Result:='';
    if NewKeyLen DIV 2 > 0 then
      begin
        for i := 0 to NewKeyLen div 2 - 1 do
          begin
            TmpHex:= Strtoint('$' + Copy(NewKey,i * 2 + 1,2));    //字符转16进制
            TmpBit:= Seed shr 8;
            Result:=Result + Char(TmpHex xor TmpBit);
            Seed:= (TmpHex + Seed) * (mb_Month + mb_Day) + mb_Year;
          end;
      end;
  except
    Result:='E';
  end;
end;

function TJM_CHILD_DATA.GetModeHash(InKey:Pchar;Count,BornSum:Integer):Integer;
var
  Len,i,k,n,p:Integer;
  Num:Array [0..3] of Byte;
  Hash:array [0..10] of char;
begin
  Result:=4;
  Len:=Length(InKey);
  if Len > 0 then
    begin
      StrCopy(Hash,InKey);
      Num[3]:=BornSum div 1000 mod 10;
      Num[2]:=BornSum div 100 mod 10;
      Num[1]:=BornSum div 10 mod 10;
      Num[0]:=BornSum mod 10;

      for i := 0 to Count - 1 do
        begin
          n:=1;
          for k := Len - 1 Downto 1 do
            begin
              Hash[n - 1] := Chr(((Ord(Hash[n-1]) - $30) - Num[1] - (Ord(Hash[n]) - $30) + $14) mod 10 + $30);
              Inc(n);
            end;
          Hash[Len - 1] := chr((Ord(Hash[Len - 1]) - $30 - Num[3] + 10) mod 10 + $30);
          p:= Len;
          if p > 2 then
            begin
              repeat
                Hash[p-1] := Chr(((Ord(Hash[p-1]) - $30 - Num[0]) - (Ord(Hash[p-2]) - $30) + $14) mod 10 + $30);
                Dec(p);
              until p = 1;
            end;
          Hash[0] := Chr((Ord(Hash[0]) - $30 - Num[2] + 10) mod 10 + $30);
        end;
      Result:= StrToInt(Hash) - 1;
    end;
end;

function TJM_CHILD_DATA.MakeCoreKey(Src:String;Month,DayMonth,Year:Cardinal):String;
var
  I,j,n: Integer;
  Buffer:Array [0..99] of Char;
  tmp:Cardinal;
begin
  j:=0;
  for I := 1 to Length(Src) do
    begin
      if ((I mod 2) = 1) then
        begin
          Buffer[j]:= Chr(StrToInt('0x'+ Copy(Src, I, 2)));
          Inc(j);
        end;
    end;
  n:=1;
  tmp:=Month;
  for i := j Downto 1 do
    begin
       Result:=Result + Chr((Ord(Buffer[n - 1]) xor (tmp div $100)) and $FF);
       tmp:= (tmp + Ord(Buffer[n - 1])) * DayMonth + Year;
       Inc(n);
    end;
end;

function TJM_CHILD_DATA.MakeF11(BaseKey: String): String;
begin
  Result:=Format('395%s5F5E56E77FC574B2CE1%s',[Copy(BaseKey,1,4),Copy(BaseKey,5,4)]);
end;

function TJM_CHILD_DATA.NewDecryptData(_JzData: string): String;
var
  Vi:Array [0..7] of Byte;
  CF11:TF11Data;
  CBorn:TBornData;
  CSex:TSexData;
  Key,Hash,CoreKey:String;
  Mode,i:Integer;

  dw_0xC8,dw_0xCC,dw_0xD0:Cardinal;
  dw_Esi,dw_Edi:Cardinal;
  dw_0x58,dw_0x5C,dw_0x60,dw_0x64,dw_0x68,dw_0x80:Cardinal;
  dw_0x94,dw_0x98:Cardinal;
  sz_0x44,szNumStr:String;
  szFinalKey,szTmpFinalKey:string;

  szStrA,szStrB,szStrC:String;
begin
  Writeln(Format('F11 = %s',[mField11]));
  CF11:=TF11Data.Create(mField11);
  CBorn:=TBornData.Create(mBorn);
  CSex:=TSexData.Create(mSex,mBorn);
  try
    Writeln(Format('Code:%s',[CF11.Key1]));
    Key:= Format('%.2d%d%.2d',[CBorn.Day,CBorn.Year,CBorn.Month]);
    FillChar(Vi,8,0);
    Hash:=DecryStr(HexToStr(CF11.Key1),Key,Vi);
    Writeln(Format('Hash = %s Size = %d',[Hash,Length(Hash)]));
    //获取算法编号
    if mSex = 1 then
      Mode:= GetModeHash(PChar(Hash),$A,CBorn.Year + CBorn.Month + CBorn.Day)
    else
      Mode:= GetModeHash(PChar(Hash),$14,CBorn.Year + CBorn.Month + CBorn.Day);
    Writeln(Format('Mode:%d',[Mode]));
    //核心秘钥
    CoreKey:= MakeCoreKey(CF11.Key2,CBorn.Month,CBorn.Day+CBorn.Month,CBorn.Year);
    Writeln(Format('CoreKey = %s',[CoreKey]));
    if CoreKey <> '' then
      begin
        dw_0xC8 := CBorn.Month div 10;
        dw_0xCC := CBorn.Month mod 10;
        dw_0xD0 := CBorn.Day mod 10;
        dw_Esi := CBorn.Day div 10;
        dw_Edi := Ord(CoreKey[6]) - $F;
        dw_0x58 := Ord(CoreKey[2]) - 7;
        dw_0x5C := Ord(CoreKey[9]) - dw_Esi - dw_0xD0;
        dw_0x60 := Ord(CoreKey[11]) + dw_Esi + dw_0xC8;
        dw_0x64 := Ord(CoreKey[5]) - dw_Esi - dw_0xC8;
        dw_0x68 := Ord(CoreKey[3]) + 3;
        dw_0x80 := CBorn.YearSum + dw_0x60;
        if dw_0x80 < $28 then dw_0x80:=dw_0x80 + $28;
        if mSex = 1 then  sz_0x44:=Copy(Format('%d%.2d%.2d',[CBorn.Year,CBorn.Month,CBorn.Day]),5,4);
        if mSex = 2 then  sz_0x44:=Copy(CBorn.TurnBack,5,4);
        szNumStr:= Copy(sz_0x44,1,2);
        dw_0x94:= StrToInt(szNumStr) + $1E;
        szNumStr:= Copy(sz_0x44,3,2);
        dw_0x98:= StrToInt(szNumStr) + $1E;
        szTmpFinalKey:='';
        case Mode of
          0:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_Esi + dw_0x58);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                end;

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x58 + dw_0x60) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
          1:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_0x58);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x64);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                end;

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x5C + dw_0x58) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
          2:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_Esi + dw_Edi);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                end;

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x60 + dw_Edi) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
          3:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x5C);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                end;
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);

              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x60 + dw_Edi) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
        else
            begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_Esi + dw_0x58);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x5C);
                end;
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x58 + dw_0x60) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
        end;

        Writeln(Format('szTmpFinalKey = %s',[szTmpFinalKey]));

        szStrA:=Copy(szTmpFinalKey,CBorn.Month,1);
        szStrB:=Copy(szTmpFinalKey,1,CBorn.Month-1);
        szStrC:=Copy(szTmpFinalKey,CBorn.Month + 1,Length(szTmpFinalKey) - CBorn.Month);
        szTmpFinalKey:=szStrA + szStrB + szStrC;

        if mSex = 1 then
           szFinalKey:= WriteAnsiChar('',dw_0x64 + dw_Esi) + szTmpFinalKey + WriteAnsiChar('',dw_0x68 + dw_0xD0)
        else
           szFinalKey:= WriteAnsiChar('',dw_0x68 + dw_Esi) + szTmpFinalKey + WriteAnsiChar('',dw_0x64 + dw_0xD0);

        if dw_0x68 mod 2 = 0 then
          begin
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_0x5C) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x68 + dw_Edi + dw_Esi) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x5C + dw_Edi + dw_0x64 + dw_0x68) div 4);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x60 + dw_0x64 + dw_0x68 + dw_Esi) div 3);
          end
        else
          begin
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_0x68 + dw_Esi) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x64 + dw_Edi) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_0x5C + dw_0x60 + dw_0x64 + dw_Esi) div 4);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_Edi + dw_0x5C) div 3);
          end;

        Writeln(Format('秘钥补齐【 %s 】',[szFinalKey]));

        szTmpFinalKey:=Copy(szFinalKey,1,Length(szFinalKey) - 5);
        szFinalKey:=Format('^~%s&)(',[szTmpFinalKey]);
        szTmpFinalKey:=Copy(szFinalKey,1,Length(szFinalKey) - 7);
        szFinalKey:=Format(')IpE%sxS#',[szTmpFinalKey]);

        Writeln(Format('最终处理后秘钥【 %s 】',[szFinalKey]));

        FillChar(Vi,8,0);
        szFinalKey:=DecryStr(HexToStr(_JzData), szFinalKey, vi);
        Result:= Format('%s\x%X',[Result,Ord(szFinalKey[1])]);
        for i := 2 to Length(szFinalKey) do
          Result:= Format('%s\x%X',[Result,Ord(szFinalKey[i])]);

      end;

  finally
    CBorn.Free;
    CSex.Free;
    CF11.Free;
  end;
end;

function TJM_CHILD_DATA.NewEncryptData(_JzData: string): String;
var
  Vi:Array [0..7] of Byte;
  CF11:TF11Data;
  CBorn:TBornData;
  CSex:TSexData;
  Key,Hash,CoreKey:String;
  Mode:Integer;

  dw_0xC8,dw_0xCC,dw_0xD0:Cardinal;
  dw_Esi,dw_Edi:Cardinal;
  dw_0x58,dw_0x5C,dw_0x60,dw_0x64,dw_0x68,dw_0x80:Cardinal;
  dw_0x94,dw_0x98:Cardinal;
  sz_0x44,szNumStr:String;
  szFinalKey,szTmpFinalKey:string;

  szStrA,szStrB,szStrC:String;
begin
  CBorn:=TBornData.Create(mBorn);
  CSex:=TSexData.Create(mSex,mBorn);

  FillChar(Vi,8,0);
  Hash:='0868';
  Key:= Format('%.2d%d%.2d',[CBorn.Day,CBorn.Year,CBorn.Month]);
  mField11:=MakeF11(Strtohex(EncryStr(Hash,Key,Vi)));
  Writeln(Format('Create F11 = %s',[mField11]));

  CF11:=TF11Data.Create(mField11);
  try
    Writeln(Format('Key1 = %s ',[CF11.Key1]));
    //获取算法编号
    if mSex = 1 then
      Mode:= GetModeHash(PChar(Hash),$A,CBorn.Year + CBorn.Month + CBorn.Day)
    else
      Mode:= GetModeHash(PChar(Hash),$14,CBorn.Year + CBorn.Month + CBorn.Day);
    Writeln(Format('Mode:%d',[Mode]));
    //核心秘钥
    CoreKey:= MakeCoreKey(CF11.Key2,CBorn.Month,CBorn.Day+CBorn.Month,CBorn.Year);
    Writeln(Format('CoreKey = %s',[CoreKey]));
    if CoreKey <> '' then
      begin
        dw_0xC8 := CBorn.Month div 10;
        dw_0xCC := CBorn.Month mod 10;
        dw_0xD0 := CBorn.Day mod 10;
        dw_Esi := CBorn.Day div 10;
        dw_Edi := Ord(CoreKey[6]) - $F;
        dw_0x58 := Ord(CoreKey[2]) - 7;
        dw_0x5C := Ord(CoreKey[9]) - dw_Esi - dw_0xD0;
        dw_0x60 := Ord(CoreKey[11]) + dw_Esi + dw_0xC8;
        dw_0x64 := Ord(CoreKey[5]) - dw_Esi - dw_0xC8;
        dw_0x68 := Ord(CoreKey[3]) + 3;
        dw_0x80 := CBorn.YearSum + dw_0x60;
        if dw_0x80 < $28 then dw_0x80:=dw_0x80 + $28;
        if mSex = 1 then  sz_0x44:=Copy(Format('%d%.2d%.2d',[CBorn.Year,CBorn.Month,CBorn.Day]),5,4);
        if mSex = 2 then  sz_0x44:=Copy(CBorn.TurnBack,5,4);
        szNumStr:= Copy(sz_0x44,1,2);
        dw_0x94:= StrToInt(szNumStr) + $1E;
        szNumStr:= Copy(sz_0x44,3,2);
        dw_0x98:= StrToInt(szNumStr) + $1E;
        szTmpFinalKey:='';
        case Mode of
          0:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_Esi + dw_0x58);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                end;

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x58 + dw_0x60) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
          1:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_0x58);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x64);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                end;

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x5C + dw_0x58) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
          2:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_Esi + dw_Edi);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                end;

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x60 + dw_Edi) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
          3:begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x5C);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_Edi);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                end;
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);

              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x60 + dw_Edi) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
        else
            begin
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_Esi + dw_0x58);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,CSex.Num + dw_0xD0 + dw_Edi);
              if mSex = 2 then
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x68);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x58);
                end
              else
                begin
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x60);
                  szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x5C);
                end;
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x94);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x98);
              szTmpFinalKey:=WriteDword(szTmpFinalKey,dw_0x58, dw_0x5C, dw_0x60, dw_Esi, dw_Edi, dw_0xC8, dw_0xCC, dw_0xD0);

              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,(dw_0x58 + dw_0x60) div 2);
              szTmpFinalKey:=WriteAnsiChar(szTmpFinalKey,dw_0x80);
            end;
        end;

        Writeln(Format('szTmpFinalKey = %s',[szTmpFinalKey]));

        szStrA:=Copy(szTmpFinalKey,CBorn.Month,1);
        szStrB:=Copy(szTmpFinalKey,1,CBorn.Month-1);
        szStrC:=Copy(szTmpFinalKey,CBorn.Month + 1,Length(szTmpFinalKey) - CBorn.Month);
        szTmpFinalKey:=szStrA + szStrB + szStrC;

        if mSex = 1 then
           szFinalKey:= WriteAnsiChar('',dw_0x64 + dw_Esi) + szTmpFinalKey + WriteAnsiChar('',dw_0x68 + dw_0xD0)
        else
           szFinalKey:= WriteAnsiChar('',dw_0x68 + dw_Esi) + szTmpFinalKey + WriteAnsiChar('',dw_0x64 + dw_0xD0);

        if dw_0x68 mod 2 = 0 then
          begin
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_0x5C) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x68 + dw_Edi + dw_Esi) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x5C + dw_Edi + dw_0x64 + dw_0x68) div 4);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x60 + dw_0x64 + dw_0x68 + dw_Esi) div 3);
          end
        else
          begin
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_0x68 + dw_Esi) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x64 + dw_Edi) div 2);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_0x5C + dw_0x60 + dw_0x64 + dw_Esi) div 4);
            szFinalKey:= WriteAnsiChar(szFinalKey,(dw_0x58 + dw_Edi + dw_0x5C) div 3);
          end;

        Writeln(Format('秘钥补齐【 %s 】',[szFinalKey]));

        szTmpFinalKey:=Copy(szFinalKey,1,Length(szFinalKey) - 5);
        szFinalKey:=Format('^~%s&)(',[szTmpFinalKey]);
        szTmpFinalKey:=Copy(szFinalKey,1,Length(szFinalKey) - 7);
        szFinalKey:=Format(')IpE%sxS#',[szTmpFinalKey]);

        Writeln(Format('最终处理后秘钥【 %s 】',[szFinalKey]));
        Writeln(Format('Code:%s',[_JzData]));
        FillChar(Vi,8,0);
        Result:=Format('%s","F11":"%s',[StrToHex(EncryStr(_JzData, szFinalKey, vi)),mField11])//StrToHex(EncryStr(_JzData, szFinalKey, vi));
      end;

  finally
    CBorn.Free;
    CSex.Free;
    CF11.Free;
  end;
end;

function TJM_CHILD_DATA.WriteAnsiChar(Src: string; C: Cardinal): String;
begin
  if Src = '' then
   Result:= Chr(C mod $100)
  else
    Result:= Src + Chr(C mod $100);
end;

function TJM_CHILD_DATA.WriteDword(Src: string; dw_0x58,dw_0x5C,dw_0x60,dw_Esi,dw_Edi,dw_0xC8,dw_0xCC,dw_0xD0: Cardinal): String;
begin
  if dw_0x58 mod 2 = 1 then
    begin
      if dw_Edi mod 2 = 1 then
        Result:= Format('%s%s%d',[Src,WriteAnsiChar('',dw_0xCC + dw_Edi),dw_0xC8])
      else
        Result:= Format('%s%s%d',[Src,WriteAnsiChar('',dw_0xC8 + dw_Edi),dw_0xCC]);

      if dw_0x60 mod 2 = 0 then
        Result:=Format('%s%s%d',[Result,WriteAnsiChar('',dw_0xD0 + dw_0x60),dw_Esi])
      else
        Result:=Format('%s%d%s',[Result,dw_Esi,WriteAnsiChar('',dw_0xD0 + dw_0x5C)]);
    end
  else
    begin
      if dw_Edi mod 2 = 1 then
        Result:= Format('%s%d%s',[Src,dw_0xD0,WriteAnsiChar('',dw_Esi + dw_Edi)])
      else
        Result:= Format('%s%s%d',[Src,WriteAnsiChar('',dw_Esi + dw_Edi),dw_0xD0]);

      if dw_0x5C mod 2 = 0 then
        Result:=Format('%s%s%d',[Result,WriteAnsiChar('',dw_0xCC + dw_0x5C),dw_0xC8])
      else
        Result:=Format('%s%d%s',[Result,dw_0xC8,WriteAnsiChar('',dw_0xCC + dw_0x5C)]);
    end;
end;

//=====================================================接口

function PhoneNumberEncrypt(_WorkMode,_Version:Integer;_SrcCode,_Key:PChar;_OutBuffer:PChar):Integer;
var
  Res:String;
begin
   if _Version = 1 then
     begin
       if _WorkMode = 0 then
         Res:=DecryStrHex(_SrcCode,g_PhoneNum.MakeDESKey(_Key))
       else
         Res:=EncryStrHex(_SrcCode,g_PhoneNum.MakeDESKey(_Key));
     end;
   if _Version = 2 then
     begin
       if _WorkMode = 0 then
         Res:=g_PhoneNum.DecryptData(_SrcCode)
       else
         Res:=g_PhoneNum.EncryptData(_SrcCode);
     end;

   StrCopy(_OutBuffer,PChar(Res));
   Result:=Length(Res);
end;



//function ChildNameEncrypt(_WorkMode:Integer;_Born,_Code:AnsiString):AnsiString;
function ChildNameEncrypt(_WorkMode:Integer;_Born,_Code:PChar;_OutBuffer:PChar):Integer;
var
  R:String;
begin
  if _WorkMode = 0 then
    R:= g_JzNameMgr.DecodeJzName(_Born,_Code)
  else
    R:= g_JzNameMgr.EncodeJzName(_Born,_Code);
  Result:=Length(R);
  if (Result <> 0) and (_OutBuffer <> nil) then
    StrCopy(_OutBuffer,PChar(R));

end;

{ TBornData }

constructor TBornData.Create(_BornStr: String);
var
  i,j:Integer;
begin
  j:=0;
  for i := 1 to Length(_BornStr) do
    begin
      // write(Inttohex(Ord(_BornStr[i]),2) + ',');
       if (Ord(_BornStr[i]) >= $30) and (Ord(_BornStr[i])<=$39) then
         begin
            BornBit[j] := Ord(_BornStr[i]) - $30;
            Inc(j);
         end;
    end;
end;

function TBornData.GetDay: Cardinal;
begin
  Result:=BornBit[6] * 10 + BornBit[7];
end;

function TBornData.GetMonth: Cardinal;
begin
  Result:=BornBit[4] * 10 + BornBit[5];
end;

function TBornData.GetTurnBack: String;
var
 i:Integer;
begin
  for i := 7 Downto 0 do
    Result:=Result + inttostr(BornBit[i]);
end;

function TBornData.GetYear: Cardinal;
begin
  Result:=BornBit[0] * 1000 + BornBit[1] * 100 + BornBit[2] * 10 + BornBit[3];
end;

function TBornData.GetYearSum: Cardinal;
begin
  Result:= BornBit[0] + BornBit[1] + BornBit[2] + BornBit[3];
end;

{ TF11Data }

constructor TF11Data.Create(_F11Str: String);
var
  Len:Integer;
begin
  Len:=Length(_F11Str);
  Key1:= Copy(_F11Str,4,8) + Copy(_F11Str,Len - 7,8);
  key2:= Copy(_F11Str,1,3) + Copy(_F11Str,12,Len - 8);
  key2:= Copy(Key2,1,Length(key2) - 8);
end;

{ TSexData }

constructor TSexData.Create(_Sex: Integer;_Born:String);
var
  Born:TBornData;
begin
  Born:=TBornData.Create(_Born);
  try
    mSex:=_Sex;

    Key[0]:= ((mSex shl 2) + (mSex shl 2) * 4) + (Born.Day div 10);
    Key[1]:= mSex * $1E + Born.Day mod 10;
    Key[2]:= ((mSex shl 2) + (mSex shl 2) * 4) + (Born.Month div 10);
    Key[3]:= ((mSex shl 3) + (mSex shl 3) * 4) + (Born.Month mod 10);

    writeln(Format('Sex -> %d Mark:%s Key:[0x%X,0x%X,0x%X,0x%X]',[
      mSex,
      GetMark(),
      Key[0],
      Key[1],
      Key[2],
      Key[3]
    ]));

  finally
    Born.Free;
  end;
end;

function TSexData.GetMark: String;
begin
  Result:='X';
  if mSex = 2 then Result:= 'Y';
end;

initialization
  Randomize;
  g_PhoneNum:=TPhoneNumberEncryt.Create;
  g_JzNameMgr:=TJzNameMgr.Create;


end.
