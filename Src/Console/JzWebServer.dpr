program JzWebServer;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  {$IFDEF WIN32}
  Winapi.Windows,
  {$ENDIF }
  {$IFDEF WIN64}
  Winapi.Windows,
  {$ENDIF }
  System.SysUtils,
  uDataModule in 'uDataModule.pas' {Dm: TDataModule},
  uLog in 'uLog.pas',
  uConfig in 'uConfig.pas',
  uInterFace in 'uInterFace.pas';

const
  C_CONFIG = '/Config.ini';
  C_SERVER_VER = '1.0';
var
  g_Config:TConfig;
  g_Module:THandle;
begin
  //创建日志
  WriteLn(Format('疫苗数据加解密Web服务 版本:%s',[C_SERVER_VER]));
  g_Log:=TLog.Create(GetCurrentDir() + '/Logs/');
  if g_Log.IsActive() then
    begin
      //加载配置
      g_Config:=TConfig.Create(GetCurrentDir() + C_CONFIG);
      if g_Config.IsActive then
        begin
          g_Log.ClientLog('配置加载完成...',[]);
          //加载模块
          //WriteLn(GetCurrentDir() + '/' + g_Config.GetCoreModule());
          g_Module:=LoadLibrary(Pchar(GetCurrentDir() + '/' + g_Config.GetCoreModule()));
          if g_Module <> 0 then
            begin
              g_Log.ClientLog('模块加载完成...[0x%X]',[g_Module]);
              if InitInterFace(g_Module) then
                begin
                  g_Log.ClientLog('接口初始化完成...',[]);
                  //启动服务
                  Dm:= TDm.Create(nil);
                  Dm.idhtpsrvr1.DefaultPort:=g_Config.GetWebServerPort();
                  Dm.idhtpsrvr1.Active:=True;
                  try
                    g_Log.ClientLog('WebServer启动,端口:%d',[g_Config.GetWebServerPort]);
                    //日志归档
                    while True do Sleep(10000);
                  finally
                    //释放
                    Dm.Free;
                    g_Log.ClientLog('Server ShutDown!',[]);
                  end;
                end;
            end
          else
            g_Log.ClientLog('加载模块文件失败！',[]);
        end
      else
        g_Log.ClientLog('读取配置文件失败！',[]);
      g_Config.Free;
    end
  else
    g_Log.ClientLog('日志创建失败！',[]);
  g_Log.Free;

end.
