unit uDataModule;

interface

uses
 {$IFDEF WIN32}
  Winapi.Windows,
 {$ENDIF}
  System.SysUtils,
  System.StrUtils,
  System.Classes, IdBaseComponent, IdComponent,
  IdCustomTCPServer, IdCustomHTTPServer, IdHTTPServer,IdContext;

type
  MyChar = Byte;

  TDm = class(TDataModule)
    idhtpsrvr1: TIdHTTPServer;
    procedure idhtpsrvr1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
  private

    Function GetValue(_Src:String):String;
    Function GetKey(_Src:String):String;

    Function OnPhoneNum(Args:TStrings):String;
    function OnChildName(Args:TStrings):String;
    function OnJzDataEncrypt(Args:TStrings):String;
  public
    { Public declarations }
  end;

var
  Dm: TDm;

implementation
  uses
    uInterFace,uLog;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}
Procedure Dbgprint(Str:String;Args:Array of const);
begin
 {$IFDEF WIN32}
  OutputDebugString(PChar(Format(Str,Args)));
 {$ENDIF}
end;

function TDm.GetKey(_Src: String): String;
var
  Tmp:TStringList;
begin
  Result:='';
  Tmp:=TStringList.Create;
  try
    Tmp.Delimiter:='=';
    Tmp.DelimitedText:=_Src;
    if Tmp.Count = 2 then
      Result:=Tmp[0];
  finally
    Tmp.Free;
  end;
end;

function TDm.GetValue(_Src: String): String;
var
  Tmp:TStringList;
begin
  Result:='';
  Tmp:=TStringList.Create;
  try
    Tmp.Delimiter:='=';
    Tmp.DelimitedText:=_Src;
    if Tmp.Count = 2 then
      Result:=Tmp[1];
  finally
    Tmp.Free;
  end;
end;

procedure TDm.idhtpsrvr1CommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  Action,RespStr:String;
begin
  AResponseInfo.ContentType := 'text/html';
  AResponseInfo.CharSet := 'utf-8';
  if UpperCase(ARequestInfo.Command) = 'POST' then
    begin
     // AResponseInfo.ContentText:=Format('%s,%s',[ARequestInfo.Document,ARequestInfo.Params[0]]);
      if GetKey(ARequestInfo.Params[0]) = 'Action' then
        begin
          Action:=LowerCase(GetValue(ARequestInfo.Params[0]));
          g_Log.ClientLog('[%s]Action = %s',[ARequestInfo.RemoteIP,Action]);
          if Action = 'phone' then
           begin
             RespStr:= OnPhoneNum(ARequestInfo.Params);
           end;
          if Action = 'name' then
           begin
             RespStr:= OnChildName(ARequestInfo.Params);
           end;
          if Action = 'jzdata' then
             RespStr:= OnJzDataEncrypt(ARequestInfo.Params);
          AResponseInfo.ContentText:=RespStr;
        end
      else
        g_Log.ClientLog('[%s]请求指令无效 %s',[ARequestInfo.RemoteIP,ARequestInfo.Params[0]]);
    end
  else
    begin
      AResponseInfo.ContentText:= ARequestInfo.Command;
    end;
end;

function TDm.OnChildName(Args: TStrings): String;
var
  i:Integer;
  m,n,r:string;
  Code:string;//Array [0..500]of Char;

  NewCode:String;
  Mode,Born:String;
  ErrorCode:Integer;

  Name:array [0..50] of Byte;
  OutBuffer:Array [0..200] of Char;
begin
  ErrorCode:=1;
  r:='';
  if Args.Count = 4 then
    begin
      for i := 1 to 3 do
       begin
         m:= GetKey(Args[i]);
         n:= GetValue(Args[i]);
         if LowerCase(m) = 'code' then  Code:= n;
         if LowerCase(m) = 'mode' then  Mode:= n;
         if LowerCase(m) = 'born' then  Born:= n;
       end;
      ErrorCode:= 2;


      try
        g_Log.ClientLog('接口调用:ChildNameEncrypt 工作模式:%s',[Mode]);
        g_Log.ClientLog('参数:[Born = %s]',[Born]);

        if LowerCase(Mode) = 'encrypt' then
          begin
            NewCode:='';
            for i := 0 to Length(Code) - 1 do
              begin
                Name[i]:= Ord(Code[i+1]);
              end;
            Name[Length(Code)]:=0;
            NewCode:=UTF8ToString(@Name);
            g_Log.ClientLog('参数:[Code = %s]',[NewCode]);
            g_InterFace.pChildNameEncrypt(1,PChar(Born),PChar(NewCode),OutBuffer);
            r:=StrPas(OutBuffer);
          end;
        if LowerCase(Mode) = 'decrypt' then
          begin
            //去掉头部 姓，直接提取后面的密文去做解密
            Name[0]:=Ord(Code[1]); Name[1]:=Ord(Code[2]);
            Name[2]:=Ord(Code[3]); Name[3]:= 0;
            NewCode:='';
            for i := 4 to Length(Code) do
              begin
                NewCode:=NewCode + Code[i];
              end;
            g_Log.ClientLog('参数:[Code = %s]',[NewCode]);
            g_InterFace.pChildNameEncrypt(0,PChar(Born),PChar(NewCode),OutBuffer);
            r:=UTF8ToString(@Name) + StrPas(OutBuffer);
          end;
        if r <> '' then ErrorCode:= 0;
        g_Log.ClientLog('返回结果:[Result = %s]',[r]);
      except
        ErrorCode:= 1000;

      end;
    end;
   //g_InterFace.pChildNameEncrypt
  Result:= Format('{"ErrorCode":%d,"Result":"%s"}',[ErrorCode,r]);
 // Result:= Format('{"ErrorCode":%d,"Result":"%s"}',[ErrorCode,AnsitoUTF8(r)]);
end;

function TDm.OnJzDataEncrypt(Args: TStrings): String;
var
  i:Integer;
  m,n,r:string;
  Code,Mode,Born,Sex,F11,V:String;
  ErrorCode,KeyIndex:Integer;
  Res:array [0..1000] of char;

  SrcCode:String;
  Src:TStringList;
begin
  ErrorCode:=1;
  KeyIndex:=0;
  r:='';
  g_Log.ClientLog('ArgCount = %d',[ Args.Count]);
  if Args.Count = 8 then
    begin
      for i := 1 to Args.Count-1 do
       begin
         m:= GetKey(Args[i]);
         n:= GetValue(Args[i]);
         if LowerCase(m) = 'code' then  Code:= n;
         if LowerCase(m) = 'mode' then  Mode:= n;
         if LowerCase(m) = 'born' then  Born:= n;
         if LowerCase(m) = 'f11' then  F11:= n;
         if LowerCase(m) = 'sex' then  Sex:= n;
         if LowerCase(m) = 'v' then  V:= n;
         if LowerCase(m) = 'idx' then  KeyIndex:= Strtoint(n);
       end;
      try
        g_Log.ClientLog('接口调用:JzDataEncrypt 工作模式:%s',[Mode]);
        g_Log.ClientLog('参数:[Version = %s]',[V]);
        g_Log.ClientLog('参数:[Born = %s]',[Born]);
        g_Log.ClientLog('参数:[Sex = %s]',[Sex]);
        g_Log.ClientLog('参数:[F11 = %s]',[F11]);
        g_Log.ClientLog('参数:[KeyIndex = %d]',[KeyIndex]);

        if Pos('\x',Code) > 0 then
          begin
            SrcCode:='';
            Src:=TStringList.Create;
            if ExtractStrings(['\'],[],PChar(Code),Src) > 0 then
              begin
                //Writeln(Format('[Src]:%s Count:%d',[Code,Src.Count]));
                for i := 1 to Src.Count do
                  begin
                    SrcCode:=SrcCode + Chr(Strtoint('0' + Src[i-1]));
                  end;
                //SrcCode:=Trim(SrcCode);
              end;
            Src.Free;
          end
        else
          SrcCode:=Code;

        g_Log.ClientLog('参数:[Code = %s]',[SrcCode]);
        if LowerCase(Mode) = 'encrypt' then
          g_InterFace.pJzDataEncrypt(1,StrToInt(V),PChar(Born),PChar(Sex),PChar(SrcCode),PChar(F11),Res,@KeyIndex);

        if LowerCase(Mode) = 'decrypt' then
          g_InterFace.pJzDataEncrypt(0,StrToInt(V),PChar(Born),PChar(Sex),PChar(SrcCode),PChar(F11),Res,@KeyIndex);
        //OutputDebugString(Res);
        r:=Res;
        if r <> '' then ErrorCode:= 0;
        g_Log.ClientLog('返回结果:[Result = %s]',[r]);
      except
        ErrorCode:= 1000;
      end;
    end;
  Result:= Format('{"ErrorCode":%d,"Result":"%s","KeyIndex":%d}',[ErrorCode,PChar(r),KeyIndex]);
  Writeln(Result);
end;

function TDm.OnPhoneNum(Args: TStrings): String;
var
  i:Integer;
  m,n,r:string;
  V,Code,SrcCode,Mode,Key:String;
  ErrorCode,Version:Integer;
  Res:Array [0..100] of char;
  Src:TStringList;
begin
  ErrorCode:=1;
  r:='';
  if Args.Count = 5 then
    begin
      for i := 1 to 4 do
       begin
         m:= GetKey(Args[i]);
         n:= GetValue(Args[i]);
         if LowerCase(m) = 'v' then  V:= n;
         if LowerCase(m) = 'code' then  Code:= n;
         if LowerCase(m) = 'mode' then  Mode:= n;
         if LowerCase(m) = 'key' then  Key:= n;
       end;
      try
        Version:=StrToInt(V);

        if Pos('\x',Code) > 0 then
          begin
            SrcCode:='';
            Src:=TStringList.Create;
            if ExtractStrings(['\'],[],PChar(Code),Src) > 0 then
              begin
                for i := 1 to Src.Count do
                  begin
                    SrcCode:=SrcCode + Chr(Strtoint('0' + Src[i-1]));
                  end;
                SrcCode:=Trim(SrcCode);
              end;
            Src.Free;
          end
        else
          SrcCode:=Code;

        ErrorCode:= 3;
        g_Log.ClientLog('接口调用:PhoneNumberEncrypt 工作模式:%s ',[Mode]);
        g_Log.ClientLog('参数:[Key = %s]',[Key]);
        g_Log.ClientLog('参数:[Code = %s]',[SrcCode]);

        if LowerCase(Mode) = 'encrypt' then
          g_InterFace.pPhoneNumberEncrypt(1,Version,PChar(SrcCode),PChar(Key),Res);
        if LowerCase(Mode) = 'decrypt' then
          g_InterFace.pPhoneNumberEncrypt(0,Version,PChar(SrcCode),PChar(Key),Res);

        r:=Res;
        if r <> '' then ErrorCode:= 0;

        g_Log.ClientLog('返回结果:[Result = %s]',[r]);
      except
        ErrorCode:=1000;
      end;
    end;
  Result:= Format('{"ErrorCode":%d,"Result":"%s"}',[ErrorCode,r]);
end;

end.
