unit uLog;

interface
uses
  system.SysUtils,
  system.classes;

const
  C_CLIENT_LOG_FILE = '/Server.log';

type
  TLog = class
    private
      mDir:String;
      mNowLogPath:String;
      mActive:Boolean;
      Procedure LogprintfEx(FileName,Str:String);
    public
      constructor Create(_LogPath:String);
      //创建当天日志目录
      Procedure ReBuildLogDir();

      function IsActive():Boolean;


      Procedure ClientLog(const Str:string;Args:Array of const);
  end;
var
  g_Log:TLog;
implementation

{ TLog }

procedure TLog.ClientLog(const Str: string; Args: array of const);
begin
  LogprintfEx(mNowLogPath + C_CLIENT_LOG_FILE,Format(Str,Args));

end;

constructor TLog.Create(_LogPath: String);
begin
  mActive:=True;
  mDir:=_LogPath;
  if Not(DirectoryExists(mDir)) then
    begin
      try
        MkDir(mDir);
      except
        Writeln(Format('创建日志根目录失败 [%s]',[mNowLogPath]));
        mActive:=False;
      end;
    end;
  ReBuildLogDir();
end;

function TLog.IsActive: Boolean;
begin
  Result:=mActive;
end;

procedure TLog.LogprintfEx(FileName, Str: String);
var
  Fs:TStringList;
  Text:String;
begin
  Fs:=TStringList.Create;
  try
    if not(FileExists((FileName))) then Fs.SaveToFile(FileName);
    Fs.LoadFromFile(FileName);
    Text:= TimeToStr(Now) + ' ' + Str;
    Fs.Add(Text);
    Fs.SaveToFile(FileName);
    Writeln(Text);
  finally
    Fs.Free;
  end;
end;

procedure TLog.ReBuildLogDir;
var
  NowTimeTick:String;
begin
  NowTimeTick:=FormatDateTime('YYYY_MM_DD',Now);
  if DirectoryExists(mDir) then
    begin
      mNowLogPath  := mDir + NowTimeTick;
      try
        if Not(DirectoryExists(mNowLogPath)) then
          begin
              MkDir(mNowLogPath);
          end;
      except
        Writeln(Format('创建日志目录失败 [%s]',[mNowLogPath]));
      end;
    end;
end;

end.
