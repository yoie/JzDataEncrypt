unit uInterFace;

interface
uses
 {$IFDEF WIN32}
  Winapi.Windows,
 {$ENDIF}
 {$IFDEF WIN64}
  Winapi.Windows,
 {$ENDIF}
  System.SysUtils;

type
  //电话加密解密
  FPhoneNumberEncrypt = function (_WorkMode,_Version:Integer;_SrcCode,_Key:PChar;_OutBuffer:PChar):Integer;
  //名字加密解密
  FChildNameEncrypt = function (_WorkMode:Integer;_Born,_Code:PChar;_OutBuffer:PChar):Integer;
  //加密的时候 _Field11 可以随机生成 长度为 38  ，0123456789ABCDEF 随机取
  FJzDataEncrypt = function (_WorkMode,_Version:Integer;_Born,_Sex,_JzData,_Field11:PChar;OutBuffer:Pchar;_pIndex:PCardinal):Integer;


  TInterFace = record
    //用户电话
    pPhoneNumberEncrypt:FPhoneNumberEncrypt;
    //用户名字
    pChildNameEncrypt:FChildNameEncrypt;
    //疫苗数据
    pJzDataEncrypt:FJzDataEncrypt;
  end;


var
  g_InterFace:TInterFace;


function InitInterFace(_Module:THandle):Boolean;



implementation

function InitInterFace(_Module:THandle):Boolean;
begin
  Result:= False;
  if _Module <> 0 then
    begin
      while True do
        begin
          g_InterFace.pPhoneNumberEncrypt:=Getprocaddress(_Module,'PhoneNumberEncrypt');
          if Not(Assigned(g_InterFace.pPhoneNumberEncrypt)) then Break;

          g_InterFace.pChildNameEncrypt:=Getprocaddress(_Module,'ChildNameEncrypt');
          if Not(Assigned(g_InterFace.pChildNameEncrypt)) then Break;

          g_InterFace.pJzDataEncrypt:=Getprocaddress(_Module,'JzDataEncrypt');
          if Not(Assigned(g_InterFace.pJzDataEncrypt)) then Break;
          Result:= True;
          break;
        end;
    end;
end;

end.
