unit uConfig;

interface
uses
  System.SysUtils,
  System.IniFiles;

type
  TConfig = class
    private
      mFile:TInifile;
      mActive:Boolean;
    public
      constructor Create(_Fs:String);
      destructor Destroy();override;


      Function GetWebServerPort():Integer;
      Function GetCoreModule():String;

      Function IsActive:Boolean;
  end;
implementation

{ TConfig }

constructor TConfig.Create(_Fs: String);
begin
  mActive:=False;
  if FileExists(_Fs) then
    begin
      mFile:=TInifile.Create(_Fs);
      mActive:=True;
    end;
end;

destructor TConfig.Destroy;
begin
  inherited;
  mFile.Free;
end;

function TConfig.GetCoreModule: String;
begin
  if Assigned(mFile) then
    Result:= mFile.ReadString('Define','Module','')
  else
    Result:='';
end;

function TConfig.GetWebServerPort: Integer;
begin
  if Assigned(mFile) then
    Result:= mFile.ReadInteger('Define','WebServerPort',7878)
  else
    Result:=7878;
end;

function TConfig.IsActive: Boolean;
begin
  Result:= mActive;
end;

end.
