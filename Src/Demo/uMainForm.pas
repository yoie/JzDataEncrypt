unit uMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;

type
  TForm1 = class(TForm)
    grp1: TGroupBox;
    grp2: TGroupBox;
    lbledt1: TLabeledEdit;
    btn1: TButton;
    grp3: TGroupBox;
    mmo1: TMemo;
    lbledt2: TLabeledEdit;
    lbledt3: TLabeledEdit;
    chk1: TCheckBox;
    idhtp1: TIdHTTP;
    lbledt4: TLabeledEdit;
    lbledt5: TLabeledEdit;
    btn2: TButton;
    lbledt6: TLabeledEdit;
    lbledt7: TLabeledEdit;
    cbb1: TComboBox;
    lbl1: TLabel;
    btn3: TButton;
    lbledt8: TLabeledEdit;
    bvl1: TBevel;
    chk2: TCheckBox;
    chk3: TCheckBox;
    lbledt9: TLabeledEdit;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure chk2Click(Sender: TObject);
    procedure chk3Click(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure Printf(Str:string;Args:Array of const);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
var
  Args:TStringList;
  Res,Temp,Rep:String;
  i:Integer;
begin
  Args:=TStringList.Create;
  try
    Args.Add('Action=Phone');
    if chk3.Checked then
      begin
        Args.Add('V=2');
         if not(chk1.Checked) then
           begin
              Temp:=lbledt2.Text;
              Rep:= Format('\x%X',[Ord(Temp[1])]);
                for i := 2 to Length(Temp) do
                  Rep:= Format('%s\x%X',[Rep,Ord(Temp[i])]);
              Args.Add(Format('Code=%s',[Rep]));
           end
         else
           Args.Add(Format('Code=%s',[lbledt2.Text]));
      end
    else
      begin
        Args.Add('V=1');
        Args.Add(Format('Code=%s',[lbledt2.Text]));
      end;
    if chk1.Checked then Args.Add('Mode=Encrypt') else Args.Add('Mode=Decrypt');

    Args.Add(Format('Key=%s',[lbledt3.Text]));
    Printf('===电话加密解密===',[]);
    for i := 0 to Args.Count - 1 do
      Printf('参数[%d] --> %s',[i+1,Args[i]]);

    Res:=idhtp1.Post(lbledt1.Text,Args);

    Printf('===返回结果===',[]);
    Printf(Res,[]);
  finally
    Args.Free;
  end;

end;

procedure TForm1.btn2Click(Sender: TObject);
var
  Args:TStringList;
  Res:String;
  i:Integer;
begin
  Args:=TStringList.Create;
  try
    Args.Add('Action=Name');
    if chk1.Checked then Args.Add('Mode=Encrypt') else Args.Add('Mode=Decrypt');
    Args.Add(Format('Code=%s',[lbledt5.Text]));
    Args.Add(Format('Born=%s',[lbledt4.Text]));
    Printf('===名字加密解密===',[]);
    for i := 0 to Args.Count - 1 do
      Printf('参数[%d] --> %s',[i+1,Args[i]]);

    Res:=idhtp1.Post(lbledt1.Text,Args);

    Printf('===返回结果===',[]);
    Printf(Res,[]);
  finally
    Args.Free;
  end;
end;

procedure TForm1.btn3Click(Sender: TObject);
var
  Args:TStringList;
  Res:String;
  i:Integer;
begin
  Args:=TStringList.Create;
  try
    Args.Add('Action=Jzdata');
    if chk1.Checked then Args.Add('Mode=Encrypt') else Args.Add('Mode=Decrypt');
    if chk2.Checked then Args.Add('V=2') else Args.Add('V=1');
    Args.Add(Format('Code=%s',[lbledt7.Text]));
    Args.Add(Format('F11=%s',[lbledt6.Text]));
    Args.Add(Format('Born=%s',[lbledt8.Text]));
    Args.Add(Format('idx=%s',[lbledt9.Text]));
    if cbb1.ItemIndex = 0 then Args.Add('Sex=1');
    if cbb1.ItemIndex = 1 then Args.Add('Sex=2');

    Printf('===接种疫苗数据加密解密===',[]);
    for i := 0 to Args.Count - 1 do
      Printf('参数[%d] --> %s',[i+1,Args[i]]);

    Res:=idhtp1.Post(lbledt1.Text,Args);

    Printf('===返回结果===',[]);
    Printf(Res,[]);
  finally
    Args.Free;
  end;
end;

procedure TForm1.chk2Click(Sender: TObject);
begin
  if chk2.Checked then
    begin
      lbledt6.Text:='395FB0537415F5E56E77FC574B2CE16CA68066';
      lbledt7.Text:='77BF11E07C122071';
      lbledt8.Text:='2010-05-11';
    end
  else
    begin
      lbledt6.Text:='306A21A283320725101069E11EAAC2A26BDB98';
      lbledt7.Text:='020000000000000082401EC986B39082473C2A6D2874F6A0';
      lbledt8.Text:='2007-02-03';
    end;
end;

procedure TForm1.chk3Click(Sender: TObject);
begin
  if chk3.Checked then
    begin
      lbledt2.Text:='bKoTb7dTczbO9Kg=';
      lbledt3.Text:='1234567890';
    end
  else
    begin
      lbledt2.Text:='2F716392E4BD29178370A18BABD3EB7B';
      lbledt3.Text:=':vbe58CFF1C3F768tN]o';
    end;
end;

procedure TForm1.Printf(Str: string; Args: array of const);
begin
  mmo1.Lines.Add(Format(Str,Args));
end;

end.
