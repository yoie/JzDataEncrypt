object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 509
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grp1: TGroupBox
    Left = 0
    Top = 0
    Width = 740
    Height = 73
    Align = alTop
    Caption = #37197#32622
    TabOrder = 0
    object lbledt1: TLabeledEdit
      Left = 82
      Top = 28
      Width = 615
      Height = 21
      EditLabel.Width = 52
      EditLabel.Height = 13
      EditLabel.Caption = #25509#21475#22320#22336':'
      LabelPosition = lpLeft
      TabOrder = 0
      Text = 'http://127.0.0.1:9988/'
    end
  end
  object grp2: TGroupBox
    Left = 0
    Top = 73
    Width = 740
    Height = 168
    Align = alTop
    Caption = #21442#25968
    TabOrder = 1
    object lbl1: TLabel
      Left = 478
      Top = 107
      Width = 59
      Height = 13
      AutoSize = False
      Caption = #24615#21035':'
    end
    object bvl1: TBevel
      Left = 2
      Top = 95
      Width = 738
      Height = 3
    end
    object btn1: TButton
      Left = 616
      Top = 29
      Width = 105
      Height = 25
      Caption = #30005#35805#21152#35299#23494
      TabOrder = 0
      OnClick = btn1Click
    end
    object lbledt2: TLabeledEdit
      Left = 82
      Top = 33
      Width = 215
      Height = 21
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = #30005#35805':'
      LabelPosition = lpLeft
      TabOrder = 1
      Text = '2F716392E4BD29178370A18BABD3EB7B'
    end
    object lbledt3: TLabeledEdit
      Left = 344
      Top = 35
      Width = 153
      Height = 21
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = #23494#38053':'
      LabelPosition = lpLeft
      TabOrder = 2
      Text = ':vbe58CFF1C3F768tN]o'
    end
    object chk1: TCheckBox
      Left = 3
      Top = 19
      Width = 56
      Height = 17
      Caption = #21152#23494
      TabOrder = 3
    end
    object lbledt4: TLabeledEdit
      Left = 82
      Top = 62
      Width = 121
      Height = 21
      EditLabel.Width = 52
      EditLabel.Height = 13
      EditLabel.Caption = #20986#29983#26085#26399':'
      LabelPosition = lpLeft
      TabOrder = 4
      Text = '2008-08-13'
    end
    object lbledt5: TLabeledEdit
      Left = 248
      Top = 62
      Width = 321
      Height = 21
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = #22995#21517':'
      LabelPosition = lpLeft
      TabOrder = 5
      Text = #20219'0700000000000000EEBB5E00935340BC15FA03489588EEF7'
    end
    object btn2: TButton
      Left = 616
      Top = 60
      Width = 105
      Height = 25
      Caption = #22995#21517#21152#35299#23494
      TabOrder = 6
      OnClick = btn2Click
    end
    object lbledt6: TLabeledEdit
      Left = 82
      Top = 132
      Width = 295
      Height = 21
      EditLabel.Width = 40
      EditLabel.Height = 13
      EditLabel.Caption = #23383#27573'11:'
      LabelPosition = lpLeft
      TabOrder = 7
      Text = '306A21A283320725101069E11EAAC2A26BDB98'
    end
    object lbledt7: TLabeledEdit
      Left = 82
      Top = 105
      Width = 383
      Height = 21
      EditLabel.Width = 76
      EditLabel.Height = 13
      EditLabel.Caption = #25509#31181#30123#33495#25968#25454':'
      LabelPosition = lpLeft
      TabOrder = 8
      Text = '020000000000000082401EC986B39082473C2A6D2874F6A0'
    end
    object cbb1: TComboBox
      Left = 520
      Top = 105
      Width = 49
      Height = 21
      ItemIndex = 0
      TabOrder = 9
      Text = #30007
      Items.Strings = (
        #30007
        #22899)
    end
    object btn3: TButton
      Left = 616
      Top = 104
      Width = 105
      Height = 25
      Caption = #25509#31181#21152#35299#23494
      TabOrder = 10
      OnClick = btn3Click
    end
    object lbledt8: TLabeledEdit
      Left = 448
      Top = 132
      Width = 81
      Height = 21
      EditLabel.Width = 52
      EditLabel.Height = 13
      EditLabel.Caption = #20986#29983#26085#26399':'
      LabelPosition = lpLeft
      TabOrder = 11
      Text = '2007-02-03'
    end
    object chk2: TCheckBox
      Left = 640
      Top = 135
      Width = 97
      Height = 17
      Caption = #20351#29992#26032#29256#26412
      TabOrder = 12
      OnClick = chk2Click
    end
    object chk3: TCheckBox
      Left = 513
      Top = 36
      Width = 80
      Height = 17
      Caption = #26032#29256#26412
      TabOrder = 13
      OnClick = chk3Click
    end
    object lbledt9: TLabeledEdit
      Left = 600
      Top = 132
      Width = 33
      Height = 21
      EditLabel.Width = 52
      EditLabel.Height = 13
      EditLabel.Caption = #31639#27861#24207#21495':'
      LabelPosition = lpLeft
      TabOrder = 14
      Text = '0'
    end
  end
  object grp3: TGroupBox
    Left = 0
    Top = 241
    Width = 740
    Height = 268
    Align = alClient
    Caption = #32467#26524
    TabOrder = 2
    object mmo1: TMemo
      Left = 2
      Top = 15
      Width = 736
      Height = 251
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object idhtp1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 696
    Top = 16
  end
end
