﻿# JzDataEncrypt

#### 项目介绍
疫苗数据库数据加密、解密


#### 软件架构

Delphi 10.2开发

#### 运行平台(支持跨平台编译)

1. 支持Linux x64

2. 支持WIN

#### 使用说明
1. 使用so/dll方式,直接调用模块(DataEncrypt.so / DataEncrypt.dll)使用导出接口
2. 使用Web方式,使用WebServer的Http接口

#### 模块接口说明 (so/dll方式)

 **1、PhoneNumberEncrypt (电话号码加解密)** 
- 申明说明
```
Typedef int (*FPhoneNumberEncrypt)(Int WorkMode,Int Version,Char* SrcCode,Char* Key,Char* OutBuffer);
```

- 参数说明



|参数|类型|值|说明|
|:---:|:---:|:---:|:---:|
|WorkMode|Int|1=加密，2=解密|工作模式|
|Version|Int|1=旧版本，2=新版本|算法版本|
|SrcCode|Char*|数据源|数据|
|Key|Char*|秘钥|秘钥,使用新版本解密时Key随意填充|
|OutBuffer|Char*|数据缓存区|存放返回数据|

- 返回说明

返回数据长度

***

**2、ChildNameEncrypt (名字加解密)** 
- 申明说明

```
Typedef int (*FChildNameEncrypt)(Int WorkMode,Char* Born,Char* SrcCode,Char* OutBuffer);
```

- 参数说明

|参数|类型|值|说明|
|:---:|:---:|:---:|:---:|
|WorkMode|Int|1=加密，2=解密|工作模式|
|Born|Char*|格式为 XXXX-XX-XX|出生年月日|
|SrcCode|Char*|数据源|数据|
|OutBuffer|Char*|数据缓存区|存放返回数据|
- 返回说明

返回数据长度

***

**3、JzDataEncrypt(接种数据加解密)***
- 申明说明

```
Typedef int (*FJzDataEncrypt)(Int WorkMode,Int Version,Char* Born,Char* Sex,Char* JzData,Char* Field11,Char* OutBuffer);
```

- 参数说明

|参数|类型|值|说明|
|:---:|:---:|:---:|:---:|
|WorkMode|Int|1=加密，2=解密|工作模式|
|Version|Int|1=旧版本，2=新版本|算法版本|
|Born|Char*|格式为 XXXX-XX-XX|出生年月日|
|Sex|Char*|1=男, 2=女|性别|
|JzData|Char*|数据源|数据|
|Field11|Char*|F11|字段11数据|
|OutBuffer|Char*|数据缓存区|存放返回数据|
- 返回说明

返回数据长度

#### Web接口说明 (POST方式)

 **1、电话号码加解密** 
- 请求参数


|参数|值|说明|
|:---:|:---:|:---:|
|Action|Phone|指令|
|V|1=旧版本，2=新版本|算法版本|
|Mode|1=加密，2=解密|工作模式|
|Code|密文 / 明文|数据源|
|Key|秘钥|秘钥,使用新版本解密时Key随意填充|

- 返回参数

|参数|值|说明|
|:---:|:---:|:---:|
|Error|错误码|参考错误码对照表|
|Result|结果|查询结果|


***

**2、名字加解密**
- 请求参数

|参数|值|说明|
|:---:|:---:|:---:|
|Action|Name|指令|
|Mode|1=加密，2=解密|工作模式|
|Born|格式为 XXXX-XX-XX|出生年月日|
|Code|密文 / 明文|数据源|

- 返回参数

|参数|值|说明|
|:---:|:---:|:---:|
|Error|错误码|参考错误码对照表|
|Result|结果|查询结果|


***

**3、疫苗数据加解密** 
- 请求参数

|参数|值|说明|
|:---:|:---:|:---:|
|Action|JzData|指令|
|Mode|1=加密，2=解密|工作模式|
|Born|格式为 XXXX-XX-XX|出生年月日|
|Sex|1=男, 2=女|性别|
|Code|密文 / 明文|数据源|
|F11|字段11|如果是加密模式,则自定义长度为38个字节的字符串,其内容为 "0123456789ABCD"随机组合|
|idx|0~4|使用的算法序号，只适用于旧版数据加密，原数据解密后会返回使用的序号，还原加密需使用相同的算法序号|
- 返回参数

|参数|值|说明|
|:---:|:---:|:---:|
|Error|错误码|参考错误码对照表|
|Result|结果|查询结果,如果是加密模式,还会带有F11的结果|
|KeyIndex|算法序号|解密后返回使用的算法序号，用于还原加密
#### 错误码对照表
|错误码|说明|
|:---:|:---:|
|0|成功|
|1|所提交参数数量不正确|
|2|参数不正确|
|1000|数据处理异常|
